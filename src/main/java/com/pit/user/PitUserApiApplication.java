package com.pit.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PitUserApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PitUserApiApplication.class, args);
    }

}
